<!DOCTYPE html>
<html lang="en" class="h-full">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Dashboard Layout</title>
    <link href="https://unpkg.com/tailwindcss@1.1.4/dist/tailwind.min.css" rel="stylesheet" />
  </head>
  <body class="bg-gray-300 h-full text-gray-600 flex flex-col">
    <div class="h-12 bg-gray-800 flex items-center justify-between shadow">
      <div class="px-4">
        <h1 class="text-gray-400 text-xl">User Name</h1>
      </div>
      <div class="flex items-center px-4">
        <div  class="text-gray-400 text-xl" >Go to site</div>
        <div class="ml-2"><img src="./images/avatar-small.jpg" alt="" class="h-8 rounded-full border" /></div>
      </div>
    </div>

    <div class="flex flex-1">
      <div class="bg-gray-800 p-6 w-64">
        <ul>
          <li>
            <a href="{{ url('menu-page-example') }}" class="flex items-center px-4 py-2 my-3 rounded hover:bg-gray-900 hover:text-gray-400 bg-gray-900 text-gray-400">
              <span class="ml-2">Example</span>
            </a>
          </li>
          <li>
            <a href="" class="flex items-center px-4 py-2 my-3 rounded hover:bg-gray-900 hover:text-gray-400">
              <span class="ml-2">Menu Item</span>
            </a>
          </li>
          <li>
            <a href="" class="flex items-center px-4 py-2 my-3 rounded hover:bg-gray-900 hover:text-gray-400">
              <span class="ml-2">Menu Item</span>
            </a>
          </li>
          <li>
            <a href="" class="flex items-center px-4 py-2 my-3 rounded hover:bg-gray-900 hover:text-gray-400">
              <span class="ml-2">Menu Item</span>
            </a>
          </li>
          <li>
            <a href="" class="flex items-center px-4 py-2 my-3 rounded hover:bg-gray-900 hover:text-gray-400">
              <span class="ml-2">Menu Item</span>
            </a>
          </li>
          <li>
            <a href="" class="flex items-center px-4 py-2 my-3 rounded hover:bg-gray-900 hover:text-gray-400">
              <span class="ml-2">Menu Item</span>
            </a>
          </li>
          <li>
            <a href="" class="flex items-center px-4 py-2 my-3 rounded hover:bg-gray-900 hover:text-gray-400">
              <span class="ml-2">Menu Item</span>
            </a>
          </li>
        </ul>
      </div>
    <!-- Main content -->
      <div class="flex-1">
        <div class="p-8">
        @yield('content')
        </div>
      </div>
    <!-- /.content-wrapper -->
    </div>
  </body>
</html>
